﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Player : MonoBehaviour
{
    public float Speed;
    public float MaxRange = 200;

    private bool isMove = true;

    public Vector3 jump;
    public float jumpForce = 2.0f;

    public bool isGrounded;
    Rigidbody rb;
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        jump = new Vector3(0.0f, 2.0f, 0.0f);
    }

    public void Restart()
    {
        transform.position = Vector3.zero;
        isMove = true;
    }

    void OnCollisionStay()
    {
        isGrounded = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (!isMove)
            return;

        Vector3 movement = Vector3.forward;

        if (Input.GetKey(KeyCode.A))
        {
            movement.x += -1;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            movement.x += 1;
        }



        transform.Translate(movement * Speed / Time.deltaTime);
        if (transform.position.x >= 12)
        {
            transform.position = new Vector3(12, transform.position.y, transform.position.z);

        }
        else if (transform.position.x <= -12)
        {
            transform.position = new Vector3(-12, transform.position.y, transform.position.z);

        }

        if (transform.position.z >= 200)
            isMove = false;

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            rb.AddForce(jump * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }

    }
}
